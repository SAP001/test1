package main

import (
	"encoding/json"
	"fmt"
	"github.com/rs/cors"
	"gitlab.com/social_network/models"
	"net/http"

	repo "gitlab.com/social_network/repo/mock"
)

func posts(w http.ResponseWriter, r *http.Request) {
	posts := repo.Repo.Posts()
	for i := range posts {
		posts[i].Comments = repo.Repo.CommentsByPost(posts[i].ID)
	}
	b, _ := json.Marshal(posts)
	w.Write(b)
}

func createPost(w http.ResponseWriter, r *http.Request) {
	post := &models.Post{}
	err := json.NewDecoder(r.Body).Decode(post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, _ := json.Marshal(repo.Repo.CreatePost(post))
	w.Write(b)
}

func delPost(w http.ResponseWriter, r *http.Request) {
	var postID int
	err := json.NewDecoder(r.Body).Decode(&postID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	repo.Repo.DelPost(postID)
	w.WriteHeader(http.StatusOK)
}

func createComment(w http.ResponseWriter, r *http.Request) {
	comment := &models.Comment{}
	err := json.NewDecoder(r.Body).Decode(comment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, _ := json.Marshal(repo.Repo.CreateComment(comment))
	w.Write(b)
}

func hello(w http.ResponseWriter, r *http.Request) {
	// ....

	fmt.Fprintf(w, "hello\n")
}

func main() {

	mux := http.NewServeMux()

	mux.HandleFunc("/posts", posts)
	mux.HandleFunc("/del_post", delPost)
	mux.HandleFunc("/create_post", createPost)

	mux.HandleFunc("/create_comment", createComment)

	handler := cors.Default().Handler(mux)
	http.ListenAndServe(":8080", handler)
}
