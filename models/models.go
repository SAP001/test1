package models

type Post struct {
	ID       int
	Author   string
	Picture  string
	Text     string
	Comments []*Comment
}

type Comment struct {
	ID     int
	PostID int
	Author string
	Text   string
}
