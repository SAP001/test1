package mock

import "gitlab.com/social_network/models"

var commentRepo = map[int]*models.Comment{
	0: {

		ID:     0,
		PostID: 0,
		Author: "Илья",
		Text:   "Действительно классный)",
	},
	1: {
		ID:     1,
		PostID: 0,
		Author: "Дима",
		Text:   "Сейчас лучше кота запосчу!",
	},
}

func (r *Repository) CommentsByPost(postID int) []*models.Comment {
	comments := make([]*models.Comment, 0)
	for _, comment := range commentRepo {
		if comment.PostID == postID {
			comments = append(comments, comment)
		}
	}
	return comments
}

func (r *Repository) CreateComment(comment *models.Comment) int {
	currID := r.lastCommentID
	commentRepo[currID] = comment
	r.lastCommentID++
	return currID
}
