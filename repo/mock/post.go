package mock

import "gitlab.com/social_network/models"

var postsRepo = map[int]*models.Post{
	0: {
		ID:      0,
		Text:    "Смотрите все, какого классного кота я нашёл!",
		Author:  "Артём",
		Picture: "https://static.kulturologia.ru/files/u18476/cote.jpg",
	},
	1: {
		ID:      1,
		Text:    "Мой кот в 5 раз лучше",
		Author:  "Дима",
		Picture: "https://images.ast.ru/upload/resize_cache/content.constructor/3cf/480_480_2/img_1608045122_3762_843_scale_1200_1_.png",
	},
	3: {
		ID:      3,
		Text:    "Мой кот в 5 раз лучше",
		Author:  "Дима",
		Picture: "https://images.ast.ru/upload/resize_cache/content.constructor/3cf/480_480_2/img_1608045122_3762_843_scale_1200_1_.png",
	},
}

func (r *Repository) Posts() []*models.Post {
	posts := make([]*models.Post, 0)
	for _, post := range postsRepo {
		posts = append(posts, post)
	}
	return posts
}

func (r *Repository) CreatePost(p *models.Post) int {
	currID := r.lastPostID
	postsRepo[currID] = p
	r.lastPostID++
	return currID
}

func (r *Repository) DelPost(id int) {
	postsRepo[id] = nil
}
