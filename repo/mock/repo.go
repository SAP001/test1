package mock

var Repo Repository

type Repository struct {
	lastPostID    int
	lastCommentID int
}
